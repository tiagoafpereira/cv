---
layout: cv
title: Tiago Pereira
---

# Tiago Pereira
Front End Developer | Tech Lead

<div id="webaddress">
<a href="mailto:tiago@tiagoafpereira.net">tiago@tiagoafpereira.net</a> |
<a href="http://www.tiagoafpereira.net">tiagoafpereira.net</a> |
<a href="https://www.linkedin.com/in/tiagoafpereira/">LinkedIn</a> |
<a href="https://gitlab.com/tiagoafpereira/cv">This CV</a>
</div>

## Previously

At Inductiva.ai writing web tools for large scale simulation on the cloud 

### Skills

- Programming Languages
  - Javascript+HTML+CSS, Typescript, Python, Java

- Frameworks, Tools and Others
  - Front-End Frameworks (Angular, VueJS, Nuxt)
  - Google Cloud
  - Linux
  - Git (Hub\|Lab), Docker, terminal, (neo)vim, JetBrains
  - Maven, Gradle
  - Jest, Vitest, JUnit

- Misc
  - [DevEx](https://about.gitlab.com/topics/devops/what-is-developer-experience/)
  - [ShapeUp](https://basecamp.com/shapeup) \| Scrum \| Kanban
  - [Extreme Programming](https://www.youtube.com/watch?v=cGuTmOUdFbo)

### Books

- [The Design Of Everyday Things](https://www.goodreads.com/book/show/840.The_Design_of_Everyday_Things)
- [The Pragmatic Programmer](https://www.goodreads.com/book/show/4099.The_Pragmatic_Programmer)
- [Accelerate](https://www.goodreads.com/book/show/35747076-accelerate)
- [Design Patterns](https://www.goodreads.com/book/show/85009.Design_Patterns)
- [and a few more...](https://www.goodreads.com/review/list/51949878-tiago-pereira?ref=nav_mybooks&shelf=read)

## Introduction

I'm a software developer with over a decade of experience designing and
implementing GUIs and interaction for applications ranging from media asset
management web applications to airline catering menu configurators.

Soon after finishing my degree (and after that a very short career in teaching) I landed my
first job in software building [Adobe Flex](https://flex.apache.org/)
applications as GUIs for near real time video processing solutions.

Over the years I've used a number of different technologies and languages but
I've always been focused on short development loops, close interaction with
product or sales teams and ensuring the best possible [User Experience](https://www.explainxkcd.com/wiki/index.php/2141:_UI_vs_UX) for the
client.

Either working on green-field projects or improving existing applications my
ultimate goal is to [delight](https://www.nngroup.com/articles/theory-user-delight/) the person using the tools I've helped create.

## Education

`2001 - 2007 | Porto, Portugal`
__University of Porto | Faculty of Engineering__

- Masters degree in Informatics and Computing Engineering

<div class="pagebreak"> </div>

## Experience

`Fev 2024 - Aug 2024 | Porto, Portugal`
__[Inductiva.ai](https://inductiva.ai/)__ | _Tech Lead_

Inductiva.ai provides a Python API for Large‑Scale Simulation on the cloud.

As a Tech Lead at Inductiva.ai I'm responsible for a range of user facing
features like public documentation, landing pages or the website itself.

My main job however is to build, maintain and manage our Nuxt based [Web
Console](https://console.inductiva.ai/) application.

It's a very talented (but small) team working on this project so we make the
best out of it by leveraging TDD, Feature Toggles, GitHub + Google Cloud
infrastructure, state
of the art linters and formatters and pair\|mob programming to build better
software sooner.

<br/>

`2021 - 2024 | Porto, Portugal`
__[Infraspeak](https://infraspeak.com)__ | _Senior Front End Developer_

Infraspeak is a provider of intelligent maintenance management solutions for
industries ranging from hospitality to healthcare.

Most of my work concentrated on the front-end experience of the company's
namesake platform. This meant extending and maintaining a VueJS codebase with a
clear focus on best practices, code reuse, TDD and performance.

Shortly before leaving I was involved in migrating a large number of components
in a semi-automated way via an [AST based Typescript codemod](https://github.com/Infraspeak/decorators-to-object-api-transpiler).

<br/>

`2017 - 2021 | Matosinhos, Portugal`
__[Glookast](https://www.glookast.com/)__ | _Senior Front End Developer_

Glookast is a global specialist in software and hardware solutions for the
broadcast industry.

A typical day at work revolved around Agile practices, heavy use of gGitlab,
Jira and Confluence. I led a small team porting a Java Swing GUI to [the
web](https://www.glookast.com/capturer).

It was quite the challenge given the near real-time nature of video recording
and playback as well as (meta)data streaming. This project relied on a TDD
approach using Jest and JUnit, PureMVC as a wrapper framework and data shuttling
used REST, SSE and WebSockets.

<br/>

`2015 - 2017 | London, UK`
__[VistaJet](https://www.vistajet.com/en/)__ | _Front End Developer_

VistaJet is a Swiss aviation holding company founded in 2004. Currently with a
fleet of 70 aircraft it operates on a global scale being able to fly customers
anywhere in the world with as little as 24 hours' notice.

While at VistaJet I mostly worked on the company's BPMS. This tool was used to
manage a number of operations from credit card payments to catering menus PDFs.
Apache Flex as UI framework and then Spring + Hibernate all the way down to the
PostgreSQL database. Most of my contributions were centered around UI/UX aspects
but it would often involve a full stack aproach.

Most of the teams were offshore, especially in Brazil, Ukraine and Pakistan.

<br/>

`2009 - 2015 | Maia, Portugal`
__[MOG](https://www.mog-technologies.com/)__ | _Front End Developer_

MOG is (also) a global specialist in software and hardware solutions for the
broadcast industry.

I started using my Adobe Flex + ActionScript skills on actual products at MOG.
This was a very exciting time since I was responsible for overhauling an entire
Java Swing application and porting it to Adobe AIR (the result was a SPA in
today's lingo).
